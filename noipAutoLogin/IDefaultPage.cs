﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace noipAutoLogin
{
    public interface IDefaultPage : IWebDriver, IDisposable
    {
        void GotoUrl(string url);
    }
}
