﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using noipAutoLogin.Properties;
using OpenQA.Selenium;

namespace noipAutoLogin
{
    public partial class Form1 : Form
    {
        private bool autoRun = false;


        public Form1(string[] args)
        {
            InitializeComponent();
            this.Text = Resources.Form1_Form1_CaptionText;
            LoadAccountInformation();

            
            if (args.Length > 0)
            {
                if (args.Contains("--clear"))
                {
                    Properties.Settings.Default.SecurityKey = String.Empty;
                    Properties.Settings.Default.AccountId = String.Empty;
                    Properties.Settings.Default.Password = String.Empty;
                    Properties.Settings.Default.Save();

                    LoadAccountInformation();
                }
                if (args.Contains("--auto") && checkAllAccountInformationExists() == true)
                {
                    autoRun = true;
                }
            }

            LoadAutorunStatus();
        }

        private void AutoLogin(string accountId, string password)
        {
            var page = new DefaultPage();

            page.Login(accountId, password);

            page.Close();
            page.Dispose();
        }

        private bool checkAllAccountInformationExists()
        {
            if (textAccountId.Text == String.Empty || textPassword.Text == String.Empty)
            {
                return false;
            }

            return true;
        }

        private void LoadAutorunStatus()
        {
            var row = new RunOnWindowsStartup();
            chkRunOnWindowsStartup.Checked = row.CheckRegistryExists(this.Text);
        }

        private void LoadAccountInformation()
        {
            textAccountId.Text = Properties.Settings.Default.AccountId;

            var password = Properties.Settings.Default.Password;
            var key = Properties.Settings.Default.SecurityKey;
            if (String.IsNullOrEmpty(password) == false && String.IsNullOrEmpty(key) == false)
            {
                password = TripleDes.Decrypt(password, true, key);
                textPassword.Text = password;
            }
        }

        private void CreateShortcutOnDesktop(string linkName)
        {
            string deskDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

            using (StreamWriter writer = new StreamWriter(deskDir + "\\" + linkName + ".url"))
            {
                string app = System.Reflection.Assembly.GetExecutingAssembly().Location;
                writer.WriteLine("[InternetShortcut]");
                writer.WriteLine("URL=file:///" + app);
                writer.WriteLine("IconIndex=0");
                string icon = app.Replace('\\', '/');
                writer.WriteLine("IconFile=" + icon);
                writer.Flush();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (autoRun)
            {
                var accountId = textAccountId.Text.Trim().ToLower();
                var password = textPassword.Text;

                AutoLogin(accountId, password);

                this.Close();
                this.Dispose();
            }
        }

        private void btnSaveAccountInformation_Click(object sender, EventArgs e)
        {
            if (textAccountId.Text == String.Empty)
            {
                MessageBox.Show(Resources.Form1_btnSaveAccountInformation_Click_Please_input_your_Account_Id_or_Email,
                    this.Text,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }
            if (textPassword.Text == String.Empty)
            {
                MessageBox.Show(Resources.Form1_btnSaveAccountInformation_Click_Please_input_your_password_,
                    this.Text,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            var tripleDes = new TripleDes();

            var key = TripleDes.GenerrateKey();
            Properties.Settings.Default.SecurityKey = key;
            
            var password = textPassword.Text;
            password = TripleDes.Encrypt(password, true, key);

            Properties.Settings.Default.AccountId = textAccountId.Text.Trim().ToLower();
            Properties.Settings.Default.Password = password;
            Properties.Settings.Default.Save();

            MessageBox.Show("Data saved.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            LoadAccountInformation();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            LoadAccountInformation();

            var accountId = textAccountId.Text.Trim().ToLower();
            var password = textPassword.Text;

            AutoLogin(accountId, password);
        }

        private void chkRunOnWindowsStartup_CheckedChanged(object sender, EventArgs e)
        {
            var row = new RunOnWindowsStartup();

            if (chkRunOnWindowsStartup.Checked)
            {
                row.AddStartupRegistry(this.Text, Application.ExecutablePath.ToString(), "--auto");
            }
            else
            {
                row.DeleteStartupRegistry(this.Text);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.AccountId = String.Empty;
            Properties.Settings.Default.Password = String.Empty;
            Properties.Settings.Default.Save();

            var row = new RunOnWindowsStartup();
            row.DeleteStartupRegistry(this.Text);

            LoadAccountInformation();
            LoadAutorunStatus();
        }

        private void btnCreateShortcutOnDesktop_Click(object sender, EventArgs e)
        {
            CreateShortcutOnDesktop("no-ip AutoLogin");
        }
    }
}
