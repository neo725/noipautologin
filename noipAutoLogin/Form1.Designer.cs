﻿namespace noipAutoLogin
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnSaveAccountInformation = new System.Windows.Forms.Button();
            this.textPassword = new System.Windows.Forms.TextBox();
            this.textAccountId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkRunOnWindowsStartup = new System.Windows.Forms.CheckBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnCreateShortcutOnDesktop = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnTest);
            this.groupBox1.Controls.Add(this.btnSaveAccountInformation);
            this.groupBox1.Controls.Add(this.textPassword);
            this.groupBox1.Controls.Add(this.textAccountId);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(38, 19);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(694, 228);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Account Information";
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(503, 158);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(169, 49);
            this.btnTest.TabIndex = 5;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnSaveAccountInformation
            // 
            this.btnSaveAccountInformation.Location = new System.Drawing.Point(288, 158);
            this.btnSaveAccountInformation.Name = "btnSaveAccountInformation";
            this.btnSaveAccountInformation.Size = new System.Drawing.Size(169, 49);
            this.btnSaveAccountInformation.TabIndex = 4;
            this.btnSaveAccountInformation.Text = "Save";
            this.btnSaveAccountInformation.UseVisualStyleBackColor = true;
            this.btnSaveAccountInformation.Click += new System.EventHandler(this.btnSaveAccountInformation_Click);
            // 
            // textPassword
            // 
            this.textPassword.Location = new System.Drawing.Point(212, 108);
            this.textPassword.Name = "textPassword";
            this.textPassword.Size = new System.Drawing.Size(460, 32);
            this.textPassword.TabIndex = 3;
            this.textPassword.UseSystemPasswordChar = true;
            // 
            // textAccountId
            // 
            this.textAccountId.Location = new System.Drawing.Point(212, 51);
            this.textAccountId.Name = "textAccountId";
            this.textAccountId.Size = new System.Drawing.Size(460, 32);
            this.textAccountId.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username or Email";
            // 
            // chkRunOnWindowsStartup
            // 
            this.chkRunOnWindowsStartup.AutoSize = true;
            this.chkRunOnWindowsStartup.Location = new System.Drawing.Point(458, 266);
            this.chkRunOnWindowsStartup.Name = "chkRunOnWindowsStartup";
            this.chkRunOnWindowsStartup.Size = new System.Drawing.Size(274, 28);
            this.chkRunOnWindowsStartup.TabIndex = 1;
            this.chkRunOnWindowsStartup.Text = "Autorun on windows startup";
            this.chkRunOnWindowsStartup.UseVisualStyleBackColor = true;
            this.chkRunOnWindowsStartup.CheckedChanged += new System.EventHandler(this.chkRunOnWindowsStartup_CheckedChanged);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(38, 255);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(136, 49);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnCreateShortcutOnDesktop
            // 
            this.btnCreateShortcutOnDesktop.Location = new System.Drawing.Point(180, 255);
            this.btnCreateShortcutOnDesktop.Name = "btnCreateShortcutOnDesktop";
            this.btnCreateShortcutOnDesktop.Size = new System.Drawing.Size(181, 49);
            this.btnCreateShortcutOnDesktop.TabIndex = 7;
            this.btnCreateShortcutOnDesktop.Text = "Desktop shortcut";
            this.btnCreateShortcutOnDesktop.UseVisualStyleBackColor = true;
            this.btnCreateShortcutOnDesktop.Click += new System.EventHandler(this.btnCreateShortcutOnDesktop_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 316);
            this.Controls.Add(this.btnCreateShortcutOnDesktop);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.chkRunOnWindowsStartup);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "no-ip Auto Login";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textPassword;
        private System.Windows.Forms.TextBox textAccountId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveAccountInformation;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.CheckBox chkRunOnWindowsStartup;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnCreateShortcutOnDesktop;
    }
}

