﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace noipAutoLogin
{
    public class RunOnWindowsStartup
    {
        // The path to the key where Windows looks for startup applications
        RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

        public bool CheckRegistryExists(string applicationName)
        {
            if (rkApp.GetValue(applicationName) == null)
            {
                return false;
            }

            return true;                  
        }

        public bool AddStartupRegistry(string applicationName, string executePath, string parameters)
        {
            if (CheckRegistryExists(applicationName) == true)
            {
                return false;
            }

            if (String.IsNullOrEmpty(parameters) == false)
            {
                executePath = String.Format("\"{0}\" {1}", executePath, parameters);
            }

            rkApp.SetValue(applicationName, executePath);

            return true;
        }

        public bool DeleteStartupRegistry(string applocationName)
        {
            try
            {
                rkApp.DeleteValue(applocationName, false);
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
