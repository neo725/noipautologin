﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace noipAutoLogin
{
    public class DefaultPage : ChromeDriver, IDefaultPage
    {
        public void Login (string accountId, string password)
        {
            this.Manage().Timeouts().SetPageLoadTimeout(new TimeSpan(0, 0, 0, 10));

            this.GotoUrl("https://www.noip.com/login");
            
            // fill accountId
            var script = String.Format("document.getElementsByName('username')[0].setAttribute('value','{0}')",
                accountId);
            this.ExecuteScript(script);

            // fill password
            script = String.Format("document.getElementsByName('password')[0].setAttribute('value','{0}')",
                password);
            this.ExecuteScript(script);

            // submit form
            var submit = this.FindElementByCssSelector("button[type=submit]");
            submit.Click();
            
            System.Threading.Thread.Sleep(5000);
        }

        public void GotoUrl(string url)
        {
            this.Navigate().GoToUrl(url);
        }
    }
}
